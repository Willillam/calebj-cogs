from discord.ext import commands
from .utils import checks
from .utils.chat_formatting import box, warning
import dice

class Dice:
    """A cog which uses the python-dice library to provide powerful dice
    expression parsing for your games!"""
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def rd(self, ctx, *, expr: str = 'd20'):
        """Evaluates a dice expression. Defaults to roll a d20.

        Valid operations include the 'mdn' dice operator, which rolls m dice
        with n sides. If m is omitted, it is assumed to be 1.
        Modifiers include basic algebra, 't' to total a result,
        's' to sort multiple rolls, '^n' to only use the n highest rolls, and
        'vn' to drop the lowest n rolls. This cog uses the dice library.

        Examples: 4d20, d100, 6d6v2, 8d4t, 4d4 + 4, 6d8^2"""

        try:
            roll = dice.roll(expr)
        except dice.ParseException:
            await self.bot.say(warning('Invalid syntax.'))
            return
        if type(roll) in [dice.elements.Integer, int]:
            res = roll
        elif len(roll) > 0:
            total = sum(roll)
            res = ', '.join(map(str, roll))
            if len(res) > 1970:
                res = '[result set too long to display]'
            if len(roll) > 1:
                res += ' (total: %s)' % total
        else:
            await self.bot.say('Empty result!')
            return
        await self.bot.say(':game_die: %s' % res)


def setup(bot):
    bot.add_cog(Dice(bot))
